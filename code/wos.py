# -*- coding: utf-8 -*-
"""
Created on Mon May 16 14:22:53 2016

@author: hricd1
"""

#import gttg
import sqlite3
from os.path import join
from os.path import exists
from collections import defaultdict
import tools
import utils as util
import time
import os
import numpy as np
import pickle
from itertools import product
from collections import Counter
import work_dirs
import pcd
from pcd import cmtycmp
from pcd.cmty import Communities as Cm
from scipy.stats import entropy

proj,scratch = work_dirs.get_locations(unixgroup='isi')

dblocation = '/l/isi/databases'
#dblocation = '/l/isi/db_test'
#dblocation = '/l/isi/db_copy'
dsets_loc   = join(scratch,'journal_networks/')
mapfname   = join(dsets_loc, 'nodename_map.pickle')
subfield_table = 'subfield_old'

def set_dblocation(new_dblocation):
    global dblocation
    dblocation = new_dblocation
def set_dsets_loc(new_dsets_loc):
    global dsets_loc
    dsets_loc = join(scratch, new_dsets_loc)

# the limit years of the dataset
# copied from wosplots after being needed here
start,end = 1898,2015

#==============================================================================
class Journal_Network(object):
    """Class representing a slice of a journal network from database."""
    
    #==========================================================================
    def __init__(self, *args, **kwargs):
        r"""

    Parameters
    ----------
    dates : tuple (optional, default: ('1800-01-01', '1909-12-31',
                                       '1800-01-01', '1909-12-31'))
        Dates used to slice the network - limits the articles used to
        produce the network to those published between given dates:
        cited_start_date,  cited_end_date,
        citing_start_date, citing_end_date
        If only two dates are given, they are used for both cited and
        citing ranges.
    slicetype : str (optional, default: 'simple')
        Defines what type of slicing are we using:
        'simple'     - time intervals for selecting citing and cited papers are
                       fixed by 'dates'
        'difference' - Limits the time difference between citing and cited
                       paper. The value of this difference is given by
                       'time_in_past' argument.
    time_in_past : int (optional, default: 10)
        If slicetype=='difference' the time difference between the cited and
        citing paper is limited by this value. Time is rouded to years, so in
        effect for time_in_past==10 the 'real' time difference goes from 9 to 10
        years (within a decade).
    only_active_journals : bool (optional, default: False)
        If True, the journals in edgelist and subfield map will be limited to
        the ones having publications in citing (outgoing, later) time slice.
    dblocation : str (optional, default: from dblocation global variable)
        Directory where sqlite database file is located.
    dsets_loc : str (optional,
                     default: '/m/cs/scratch/isi/darko/journal_networks/')
        Directory where the produced networks should be saved.
    dsetname : str (optional, default: dates in the form "%s..%s--%s..%s"
        Name of the dataset, also the name of the dataset directory.
    dbtype : str (optional, default: 'full')
        Version (filename) of the dataset file.
        'test' - Test version of dataset.
        'full' - Full version of dataset.
        other  - Provided string will be used verbatim.
    dry : bool (optional, default: False)
        If set, no query will be run.
    """
        self.dates = ['1800-01-01','1909-12-31','1800-01-01','1909-12-31']
        if args and len(args[0]) in (2,4):
            self.dates = list(args[0]*(4/len(args[0])))
        self.slicetype      = 'simple'
        self.time_in_past   = 10
        self.only_active_journals = False
        self.dblocation     = dblocation
        self.dsets_loc      = dsets_loc
        self.dsetname       = self.date_ranges_short
        self.dbtype         = 'full'
        self.dry            = False
        self._links = None
        self._all_journals = None
        self._journal_dict = None
        self._sf_dict = None
        self._j_title = None
        self._papers = None
        self._number_of_citations = None
        self._comments = ''
        
        # all object variables can be set via kwargs
        for key,value in kwargs.iteritems():
            if not key.startswith('_'):  # prevent the attempts to set hidden attrs
                if hasattr(self, key):
                    setattr(self, key, value)
                else:
                    print "unknown attribute '%s'"%key
        
        # update self.dates if necessary
        if self.slicetype=='difference':
            self.dates[0] = str(int(self.dates[2][:4])-self.time_in_past)+\
                            self.dates[2][4:]
        
        if self._j_title==None:
            # set default if not already set
            self.j_title = True
        
        self._db = sqlite3.connect(self._dbfname)
        self._cur = self._db.cursor()
        self._check_indices()
        print 'db connection opened (%s)'%self._dbfname
        
        if self.j_title:
            self._attach_journal()
        
    #==========================================================================
    @property
    def _dbfname(self):
        if self.dbtype=='test':
            fname = 'isi-test-orig.sqlite'
        elif self.dbtype=='full':
            fname = 'isi-orig.sqlite'
        else:
            fname = self.dbtype
        fname = join(self.dblocation, fname)
        if exists(fname):
            return fname
        else:
            raise IOError('Unknown database file: %s'%fname)
    @property
    def j_title(self):
        return self._j_title
    @j_title.setter
    def j_title(self, value):
        if self._j_title==None:
            self._j_title = value
        else:
            raise RuntimeError, "j_title attribute cannot be set after the object has been initialized"
    @property
    def _q_FROM(self):
        q = r"""FROM
  citation c JOIN article a1 ON c.pid=a1.pid
             JOIN article a2 ON c.cited_pid=a2.pid
WHERE (a1.date BETWEEN date('{2}') AND date('{3}'))""".format(*self.dates)
        if self.slicetype=='simple':
            return q + """
  AND (a2.date BETWEEN date('{0}') AND date('{1}'))""".format(*self.dates)
        elif self.slicetype=='difference':
            return q + """
  AND (a2.date BETWEEN date(a1.date, 'start of year', '-{0} years') AND a1.date)""".format(self.time_in_past-1)
        else:
            raise RuntimeError, "'%s' is not a valid slicetype option"%self.slicetype
    @property
    def _link_query(self):
        title_SELECT = r"""SELECT replace(j1.j_title,' ','_'), replace(j2.j_title,' ','_'), links.mult"""
        jid_SELECT = r"""SELECT a1.jid AS jid1, a2.jid AS jid2, count(*) AS mult"""
        q_GROUP = r"""GROUP BY a1.jid,a2.jid"""
        title_JOINS = r"""JOIN journal j1 ON links.jid1=j1.jid
JOIN journal j2 ON links.jid2=j2.jid"""
        jid_query = '\n'.join((jid_SELECT, self._q_FROM, q_GROUP))
        if self.j_title:
            return '\n'.join((title_SELECT,
                              'FROM (',
                              util.indent(jid_query,2),
                              ') AS links',
                              title_JOINS))
        return jid_query
    @property
    def _subfield_query(self):
        title_SELECT = r"""SELECT replace(journal.j_title,' ','_'), replace(subfield.sf,' ','_')"""
        jid_SELECT = r"""SELECT nodes.jid, subfield.sf"""
        jid_JOIN = r"""JOIN %s AS subfield ON nodes.jid=subfield.jid"""%subfield_table
        title_JOIN = r"""JOIN journal ON nodes.jid=journal.jid"""
        q_common = '\n'.join(('FROM (',
                              util.indent('\n'.join(('SELECT a1.jid',
                                                     self._q_FROM,
                                                     'UNION\nSELECT a2.jid',
                                                     self._q_FROM)),2),
                               ') AS nodes',
                               jid_JOIN))
        if self.j_title:
            return '\n'.join((title_SELECT, q_common, title_JOIN))
        return '\n'.join((jid_SELECT, q_common))
    @property
    def date_ranges_pretty(self):
        return "cited:  %s..%s <--\nciting: %s..%s -->"%(tuple(self.dates))
    @property
    def date_ranges_short(self):
        return "%s..%s--%s..%s"%(tuple(self.dates))
    
    #==========================================================================
    @property
    def links(self):
        """A network of journals (weighted, directed)."""
        if self._links==None:
            if self.dry:
                print 'link query:\n',util.indent(self._link_query,2)
                return []
            else:
                self._cur.execute(self._link_query)
                self._links = self._cur.fetchall()
                if self.only_active_journals:
                    active_journals = set(self.papers.keys()) | set((u'None',u'NoData'))
                    self._links = [link for link in self._links if link[1] in active_journals]
        return self._links
    @property
    def all_journals(self):
        if self._all_journals==None:
            self._all_journals = set()
            for link in self.links:
                self._all_journals.update(link[:2])
        return self._all_journals
    @property
    def journal_dict(self):
        if self._journal_dict==None:
            if self.dry:
                return {}
            self._make_subfield_dict()
        return self._journal_dict
    @property
    def sf_dict(self):
        if self._sf_dict==None:
            self._make_subfield_dict()
        return self._sf_dict
    @property
    def papers(self):
        if self._papers==None:
            self._make_papers_dict()
        return self._papers
    @property
    def N(self):
        return len(self.all_journals)
    @property
    def number_of_links(self):
        """Number of links."""
        return len(self.links)
    E = number_of_links
    @property
    def number_of_citations(self):
        """The strength of all links == number of citations."""
        if self._number_of_citations==None:
            c = 0
            for link in self.links:
                c += link[2]
            self._number_of_citations = c
        return self._number_of_citations
    C = number_of_citations
    @property
    def number_of_subfields(self):
        if self.sf_dict==None:
            return 0
        return len(self.sf_dict)
    sf = number_of_subfields
    
    #==========================================================================
    def _attach_journal(self):
        dbs = self._cur.execute("PRAGMA database_list").fetchall()
        dbs = [_[1] for _ in dbs]
        if 'journals' not in dbs:
            self._cur.execute("ATTACH DATABASE '%s' as 'journals'"\
                %join(self.dblocation, 'journal.sqlite'))
    
    #==========================================================================
    def _make_subfield_dict(self):
        """Makes a dict for journal<->subfield."""
        self._attach_journal()
        if self.dry:
            print 'databases:\n',util.indent(str(self._cur.execute("PRAGMA database_list").fetchall()),2)
            print 'subfield query:\n',util.indent(self._subfield_query,2)
        else:
            self._cur.execute(self._subfield_query)
            self._journal_dict = defaultdict(set)
            self._sf_dict = defaultdict(set)
            # jid: j_title
            # 0  : u'None'   ('None' j_title in table: issue<->journal_title)
            # -1 : u'NoData'   (missing link in table: issue<->journal_title)
            if self.j_title:
                missings = [(u'None',u'None'),(u'NoData',u'NoData')]
            else:
                missings = [(0,'0'),(1,'1')]
            if self.only_active_journals:
                active_journals = set(self.papers.keys()) | set((u'None',u'NoData'))
            for links in (self._cur, missings):
                for journal,sf in links:
                    if self.only_active_journals and journal not in active_journals:
                        continue
                    self._journal_dict[journal].add(sf)
                    self._sf_dict[sf].add(journal)
        
    #==========================================================================
    def _check_indices(self):
        if not self._cur.execute("SELECT sql FROM sqlite_master WHERE type = 'index' and sql LIKE '%ON citation (pid)'"):
            print 'All querries will be slow because index is not created on citation.pid!'
    
    #==========================================================================
    def info(self):
        print self._info()
    def _info(self):
        """Prints basic info about the network."""
        info  = "date ranges:\n%s\n"%(util.indent(self.date_ranges_pretty,3),)
        info += "journals:  %d (%d with subfield)\nlinks:     %d\ncitations: %d\nsubfields: %d\n"\
            %(self.N, len(self.journal_dict), self.E, self.C, self.sf)
        info += "weights distribution:\n"
        if self.dry:
            values,borders = [0],[0,10]
        else:
            weights_dist = [_[2] for _ in self.links]
            max_power = int(np.log10(max(weights_dist)))
            values,borders = np.histogram(weights_dist,
                             bins=np.logspace(0,max_power+1,max_power+2))
            borders[-1] = max(weights_dist)
        for i in range(len(values)):
            info += "%d\t%d..%d\n"%(values[i],borders[i],borders[i+1]-1)
        return info
    
    #==========================================================================
    def keep_tagged_journals(self):
        """Keeps only that have subfields."""
        old_info = "Before the removal of journals without subfields:\n%s"%(util.indent(self._info(),2),)
        print old_info
        nones = ('None','NoData')
        clean_links = []
        tagged_journals = [j for j in self.journal_dict if j not in nones]
        for link in self.links:
            if link[0] in tagged_journals and link[1] in tagged_journals:
                clean_links.append(link)
        self._links = clean_links
        for non in nones:
            del self._sf_dict[non]
            del self._journal_dict[non]
        self._all_journals = None
        # some tagged journals might be left without edges so we need to remove
        # them from the mappings
        for j in set(self._journal_dict).difference(self.all_journals):
            del self._journal_dict[j]
        self._sf_dict = defaultdict(set)
        for j,sfs in self._journal_dict.iteritems():
            for sf in sfs:
                self._sf_dict[sf].add(j)
        self._number_of_citations = None
        print "After the removal:"
        self.info()
        self._comments = old_info

    #==========================================================================
    def _make_papers_dict(self):
        papers_query = r"""SELECT replace(journal.j_title,' ','_'), count(*)
FROM
  journal JOIN article ON journal.jid=article.jid
  WHERE (article.date BETWEEN date('{2}') AND date('{3}'))
  GROUP BY article.jid""".format(*self.dates)
        if self.dry:
            print papers_query
        else:
            self._cur.execute(papers_query)
            self._papers = {str(j):p for j,p in self._cur}

#==============================================================================
def save_dset(net, dry=True):
    """Saves links and categories as a new dset."""
    directory = join(net.dsets_loc, net.dsetname)
    edgelist_fname = join(directory,'graph-orig.txt')
    comms_fname = join(directory,'comm-orig-jnet.txt')
    
    if not dry:
        os.mkdir(directory)
    
    # edgelist file
    comment = r"""Directed and weighted edgelist of citations between journals for periods:
%s
slicetype: %s
time_in_past: %s
only_active_journals: %s
Date created: %s
Created from citation data in file '%s'
with query:
%s"""%(util.indent(net.date_ranges_pretty,2),
       str(net.slicetype), str(net.time_in_past), str(net.only_active_journals),
       time.ctime(), net._dbfname,
        util.indent(net._link_query,2))
    if net._comments:
        comment += "%s"%net._comments
    if not dry:
        tools.save_dict_to_commlist(edgelist_fname, net.links,
                                    comment=comment)
    print 'saved: %s\ncomment:\n%s'%(edgelist_fname,comment)
    
    # community file
    comment = r"""Community file for edgelist: %s
Date created: %s
Created from journal<->subfield associations in journal.sqlite
with query:
%s"""%(edgelist_fname, time.ctime(),
        util.indent(net._subfield_query,2))
    if not dry:
        tools.save_dict_to_commlist(comms_fname, net.sf_dict, comment=comment)
    print '\nsaved: %s\ncomment:\n%s'%(comms_fname,comment)
    if not dry:
        tools.save_dict_to_commlist(comms_fname+'.names', net.sf_dict.keys())
    print '\nsaved: %s\n'%(comms_fname+'.names',)

def save_dsets(nets, dry=True):
    """Saves a list of dsets."""
    for net in nets:
        save_dset(net, dry=dry)

#==============================================================================
def filter_net(g, alpha, ret_copy=False, directed=False, keep_weights=False,
               keep_min_spanning_tree=False):
    """Filters and simplifies the network keeping only the "significant" edges.

    Parameters
    ----------
    g : :class:`gt.Graph`
        Filtering is done "in place" on this network.
    alpha : float
        Value of alpha used for thresholding.
    ret_copy : bool (optional, default: False)
        Return new graph or change the existing one?
    directed : bool (optional, default: False)
        Turn into directed graph?
    keep_weights : bool (optional, default: False)
        Keep edge weights?

    Notes
    -----
    The thresholding implemented here is 'copied' from Mikko Kivelä's code:
        >>>
        newnet=pynet.SymmNet()
        for node in net:
            s=net[node].strength()
            k=net[node].deg()
            for neigh in net[node]:
                w=net[node,neigh]
                if (1-w/s)**(k-1)<threshold:
                    newnet[node,neigh]=w
        netext.copyNodeProperties(net,newnet)
    
        return newnet
        <<<
    which is an implementation of the method from:
        Extracting the multiscale backbone of complex weighted networks
        www.pnas.org/cgi/doi/10.1073/pnas.0808904106
    """
    import gttools as gtt
    if ret_copy:
        g = g.copy()
    gtt.gt.remove_self_loops(g)
    if keep_min_spanning_tree:
        tree = gtt.gt.min_spanning_tree(g, weights=g.ep.weight)
    to_remove = g.new_edge_property("bool", True)
    for v in g.vertices():
        # out direction
        s = v.out_degree(weight=g.ep.weight)
        k = v.out_degree()
        for nbr in v.out_neighbours():
            e = g.edge(v,nbr)
            if k==1:
                # nbr is v's only out neighbour
                if nbr.in_degree()==1:
                    # a special case, keep the edge
                    to_remove[e] = False
                # this is nbr's problem
                break
            w = float(g.ep.weight[e])
            # a regular case: v's problem
            if (1-w/s)**(k-1)<alpha:
                to_remove[e] = False
        # in direction
        s = v.in_degree(weight=g.ep.weight)
        k = v.in_degree()
        for nbr in v.in_neighbours():
            e = g.edge(nbr,v)
            if k==1:
                if nbr.out_degree()>1:
                    break
                else:
                    to_remove[e] = False
                    break
            w = float(g.ep.weight[e])
            if (1-w/s)**(k-1)<alpha:
                to_remove[e] = False
    if keep_min_spanning_tree:
        to_remove.a = gtt.np.logical_and(to_remove.a, gtt.np.logical_not(tree.a))
    gtt.gt.remove_labeled_edges(g, to_remove)
    
    # some cleaning
    g.set_directed(directed)
    if not directed:
        gtt.gt.remove_parallel_edges(g)
    if not keep_weights:
        g.ep.weight.set_value(1)
    if ret_copy:
        return g
    
def collect_alphas(g):
    """Collects alphas of all edges in directed graph g.

    Paramaters
    ----------
    g : :class:`gt.Graph`
        The input graph. If 'weight' edge property is not present, the following
        ep will be checked as well: 'count'

    Returns
    -------
    alphas : list
        List of tuples:
            (source_edge, target_edge, edge_weight, alpha_value)
        sorted by increasing alpha values.
    """
    try:
        weight = g.ep.weight
    except KeyError:
        weight = g.ep.count
    def insert_alpha(s,t,w,a):
        alphas[(s,t)] = (w, min(alphas.get((s,t), 1.1), a))
    alphas = {}
    for v in g.vertices():
        # out direction
        s = v.out_degree(weight=weight)
        k = v.out_degree()
        for nbr in v.out_neighbours():
            if v==nbr:
                continue
            e = g.edge(v,nbr)
            w = float(weight[e])
            if k==1:
                # nbr is v's only out neighbour
                if nbr.in_degree()==1:
                    # a hack that prevents this link from being deleted
                    insert_alpha(int(v),int(nbr), w, .0)
                # this is nbr's problem
                break
            # a regular case: v's problem
            insert_alpha(int(v),int(nbr), w, (1-w/s)**(k-1))
        # in direction
        s = v.in_degree(weight=weight)
        k = v.in_degree()
        for nbr in v.in_neighbours():
            if v==nbr:
                continue
            e = g.edge(nbr,v)
            w = float(weight[e])
            if k==1:
                if nbr.out_degree()>1:
                    break
                else:
                    insert_alpha(int(nbr),int(v), w, .0)
                    break
            insert_alpha(int(nbr),int(v), w, (1-w/s)**(k-1))
    return sorted([(k[0],k[1],v[0],v[1]) for k,v in alphas.iteritems()], key=lambda a:a[3])
    

def make_thresholded_network(old_dsetname, alpha, lcc=False, save=False,
                             **kwargs):
    """Loads existing network, filters it, and saves under a new name.

    Parameters
    ----------
    old_dsetname : str
        Name of the old dataset to be used for making a thresholded one.
    alpha : float
        Value of alpha used for thresholding.
    lcc : bool (optional, default: False)
        Keep only largest connected component?
    save : bool (optional, default: False)
        Save the new graph to disk?
    kwargs
        The rest of the the keyword arguments are passed to Prediction class.

    Returns
    -------
    new_dset : :class:`gttools.Prediciton`
        The new, thresholded network.

    Notes
    -----
    The actual thresholding is implemented in function filter_net.
    """
    import gttools as gtt
    dset = gtt.Prediction(old_dsetname, removed_nodes=0, **kwargs)
    assert dset.directed, "undirected networks not yet implemented!"
    g = dset.g
    num_old_edges = g.num_edges()
    max_edges = g.num_vertices()*(g.num_vertices()-1)/2.
    # LCCs
    comps = gtt.gt.label_components(g, directed=False)
    old_lccs = reversed(["%6.d  %d"%(i,v) for i,v in enumerate(np.bincount(np.array(comps[1], dtype=int))) if v])
    
    if dset.directed:
        # do the thresholding
        filter_net(g,alpha)
    
    # LCC stats collection
    comps = gtt.gt.label_components(g, directed=False)
    new_lccs = reversed(["%6.d  %d"%(i,v) for i,v in enumerate(np.bincount(np.array(comps[1], dtype=int))) if v])
    message = "make_thresholded_network:\nretained %.1f%% of old edges (%d/%d)\nedge densities: new: %.2f%%, old: %.2f%%\n"%\
        (float(g.num_edges()*100)/num_old_edges, g.num_edges() ,num_old_edges,
         g.num_edges()/max_edges*100 ,num_old_edges/max_edges*100)
    message += "old LCC size distribution:\n  size  count\n-------------\n%s\n"%'\n'.join(old_lccs)
    message += "new LCC size distribution:\n  size  count\n-------------\n%s"%'\n'.join(new_lccs)
    print "\n",message
    
    # LCC filtering
    if lcc:
        dset.lcc = True
    
    g.gp['notes'] = notes = g.gp.get('notes', g.new_graph_property("string"))

    # amend non-dated note with date
    with open(dset.g_fname+".info", 'r') as infofile:
        lines = infofile.read().split('\n')
    date = ''
    for l in lines:
        if l.startswith('['):
            date = l
        if "multiple edges --> 'weight' edge property" in l:
            l = "  %s %s"%(date,l)
            break
    if type(notes) is str:
        notes = notes.replace("\n  multiple edges --> 'weight' edge property",
                          "\n  %s multiple edges --> 'weight' edge property"%date)
    else:
        notes = ''
    
    notes += '\n  '+"[%s] network thresholded with alpha=%f"%(time.ctime(), alpha)
    g.gp['notes'] = notes
#    print '[make_thresholded_network]\n',notes
    
    # save new graph
    dset.dsetname = "{}_th{:.3g}".format(old_dsetname,alpha)
    if save:
        dset.saveg(notes=message, overwrite=True)
    return dset

#==============================================================================
def copy_removed_nodes(source_dset, source_samples, target_dsets,
                       replace=False, archive=True, dry=True):
    """Copies removed_nodes set to another dataset, based on node names.

    This allows to have the same set of removed_nodes in different datasets.
    Function will fail if a node does not exist in target dset.
    """
    import gttools as gtt
    from shutil import move
    from shutil import rmtree
    jnp = gtt.Prediction(source_dset)
    for tds in target_dsets:
        tjnp = gtt.Prediction(tds)
        arch = join(gtt.dsets_loc, tds, 'archive')
        i=1
        while os.path.exists(arch):
            arch = ''.join(arch.split('archive')[:-1])+'archive_%d'%i
            i+=1
        for source_sample in source_samples:
            jnp.sample_name  = source_sample
            tjnp.sample_name = source_sample
            removed_nodes_names = [jnp.g.vp.name[jnp.g.vertex(v)] for v in jnp.removed_nodes]
            nodemap = {tjnp.g.vp.name[v]:int(v) for v in tjnp.g.vertices()}
            target_removed_nodes = [nodemap[v] for v in removed_nodes_names]
            target_sample_loc = join(gtt.dsets_loc, tds, source_sample)
            if os.path.exists(target_sample_loc):
                if replace:
                    if archive:
                        print 'archiving:',target_sample_loc
                        print '       to:',arch
                        if not dry:
                            if not os.path.exists(arch):
                                os.mkdir(arch)
                            move(target_sample_loc, arch)
                    else:
                        print 'deleting old sample',target_sample_loc
                    if not dry: rmtree(target_sample_loc, ignore_errors=True)
                else:
                    print 'dir:\n%s\nexist, skipping'%target_sample_loc
                    continue
                print 'creating dir:',target_sample_loc
            if not dry:
                os.mkdir(target_sample_loc)
            if not dry:
                pickle.dump(target_removed_nodes,
                        open(join(target_sample_loc,'removed_nodes.pickle'),'w'))
            print 'saved:       ',join(target_sample_loc,'removed_nodes.pickle')

#==============================================================================
def make_journal_map():
    """Queries journal.sqlite for jid<->j_title links and saves dict to disk.

    This one is not used, as jid<->j_title mapping is done internally,
    during the loading of the network from database."""
    jdbfname = join(dblocation, 'journal.sqlite')
    if not exists(jdbfname):
        raise IOError('Unknown journal db file: %s'%jdbfname)
    jdb = sqlite3.connect(jdbfname)
    cur = jdb.cursor()
    cur.execute("SELECT * FROM journal")
    nodename_map = {l[0]:l[1] for l in cur}
    nodename_map[-1] = "Orphan issue"
    pickle.dump(nodename_map, open(mapfname, 'w'))
    print "nodename map dict pickled to '%s'"%mapfname

#==============================================================================
def make_fields_comm_file(dset_name, dsets_loc, field_map_file,
                          subfield_comm_fname='comm-orig-jnet.txt',
                          field_comm_fname='comm-orig-flds.txt',
                          strict=False):
    """Ad-hoc function for making field communities from subfields communities.
    
    Uses existing subfields community file and a map file extracted from database.
    Saves new community files in dset's folder.
    
    Arguments
    ---------
    strict : bool (optional, default: False)
        Stop on missing subfield:field mapping?
    """
    # loading the subfield -> field map
    field_map = {}
    with open(field_map_file,'r') as fmf:
        lines = fmf.read().splitlines()
        for line in lines:
            line = line.split('|')
            field_map[line[1].replace(' ','_')] = line[0]
    
    # populating existing subfields communities
    sf_comm_fname = join(dsets_loc, dset_name, subfield_comm_fname)
    sf_commf = open(sf_comm_fname, 'r')
    sf_commfn = open(sf_comm_fname+'.names', 'r')
    sf_comms = {}
    sf_comms_lines = [l for l in sf_commf.read().splitlines() if l[0]!='#']
    sfns = [l for l in sf_commfn.read().splitlines() if l[0]!='#']
    sf_commf.close()
    sf_commfn.close()
    for journals,sfn in zip(sf_comms_lines, sfns):
        sf_comms[sfn] = set(journals.split(' '))
    
    # making fields communities
    f_comms = defaultdict(set)
    missing_sfs = set()
    for sf,journals in sf_comms.iteritems():
        try:
            f_comms[field_map[sf]].update(journals)
        except KeyError:
            missing_sfs.add(sf)
            f_comms[sf].update(journals)
    if missing_sfs:
        print "subfields not present in the mapping:\n",missing_sfs
        if strict:
            print "strict option is on, nothing is saved!"
            return
    
    # saving the new fields comms file
    f_comm_fname = join(dsets_loc, dset_name, field_comm_fname)
    comment = r"""Community file created by agglomerating subfields into fields.
Date created: %s
Subfields file used: %s
field<->subfield map file: %s"""%(time.ctime(), sf_comm_fname, field_map_file)
    print comment
    tools.save_dict_to_commlist(f_comm_fname, f_comms, comment)
    tools.save_dict_to_commlist(f_comm_fname+'.names', f_comms.keys())
    print "saved: %s\nand:   %s\n"%(f_comm_fname,f_comm_fname+'.names')

#==============================================================================
def make_decades(ranges=[1890]+range(1910,1990,10)+range(1985,2015,5), dry=True,
                 name_appendix=''):
    """Creates networks of journals for each decade.

    Uses the numbers in ranges as limits for the decades.
    """
#    nets = []
    for i in range(len(ranges)-1):
        decade_start = ranges[i]
        decade_end = ranges[i+1]-1
        decade = ('%d-01-01'%decade_start, '%d-12-31'%(decade_end))
        if decade_start<1900 and decade_end==1909:
            name = '1900s'
        elif ranges[i+1]-ranges[i]==10:
            name = '%ds'%decade_start
        else:
            name = '%d-%d'%(ranges[i], ranges[i+1])
        name += name_appendix
        net = Journal_Network(decade, dsetname=name, dry=dry)
#        print '\n',name
#        net.info()
        yield net
#        nets.append(net)
#    yield nets
#=================================
def make_decades2(start_years=range(1900,1971), decade_length=10, dry=True):
    """Creates networks of journals for each decade.

    Parameters
    ----------
    start_years : list
        The starting years of time periods (decades).
    decade_length : int
        The length of time periods.
    """
    for sy in start_years:
        decade_start = sy
        decade_end = sy+decade_length
        decade = ('%d-01-01'%decade_start, '%d-12-31'%(decade_end-1))
        if decade_length==10:
            name = '%ds'%decade_start
        else:
            name = '%d-%d'%(decade_start, decade_end)
        net = Journal_Network(decade, dsetname=name, dry=dry)
        yield net
#=================================
def make_decades3(time_slices, dsetnames, slicetype='simple',
                  time_in_past='slice', only_active_journals=False,
                  dry=True):
    """Creates networks of journals for each decade.

    Parameters
    ----------
    time_slices : list
        List of tuples defining the time slices. For instance wosplots.slices
    dsetnames : list
        List of dsetnames. Should correspond to time_slices.
    slicetype : str (optional, default: 'simple')
        Slicetype argument from class Journal_Network. Options:
        'simple'     - time intervals for selecting citing and cited papers are
                       fixed by values in time_slices.
        'difference' - Limits the time difference between citing and cited
                       paper. The value of this difference is given by
                       'time_in_past' argument.
    time_in_past : int, str (optional, default: 'slice')
        If int: defines fixed time difference.
        If 'slice': time difference is the length of slice.
    only_active_journals : bool (optional, default: False)
        If True, do not add journals that are not active in citing time slice.
    """
    tip = time_in_past
    for ts,dset in zip(time_slices,dsetnames):
        time_slice = ('%d-01-01'%ts[0], '%d-12-31'%(ts[1]-1))
        if time_in_past=='slice':
            tip = ts[1]-ts[0]
        net = Journal_Network(time_slice, dsetname=dset,
            slicetype=slicetype, only_active_journals=only_active_journals,
            time_in_past=tip,
            dry=dry)
        yield net
#==============================================================================
def save_papers_dict(nets, save_often=True):
    """Saves dict of journals:number_of_papers for each slice to disk.

    Parameters
    ----------
    nets : list
        List of Journal_Network objects.
    save_often : bool (optional, defautl: True)
        Save to disk after each dataset?
    """
    dict_fname = join(dsets_loc, 'papers.pickle')
    try:
        papers = pickle.load(open(dict_fname, 'r'))
        print "loaded dict from file"
    except IOError:
        print "error loading dict from file, will create a new one"
        papers = {}
    for net in nets:
        if net.dsetname in papers:
            continue
        print "collecting papers for dset:",net.dsetname
        papers[net.dsetname] = net.papers
        print "...collected"
        if save_often:
            pickle.dump(papers, open(dict_fname, 'w'))
            print "dict saved to file '%s'"%dict_fname
    if not save_often:
        pickle.dump(papers, open(dict_fname, 'w'))
        print "dict saved to file '%s'"%dict_fname
#=================================
def save_papers_dict2(jya_fname='journal_year_articles.pickle',
                      yja_fname='year_journal_articles.pickle',
                      mat_fname='journal_x_year.pickle'):
    """Saves dict of journals:year:number_of_papers and matrix journal_x_year to disk.

    Parameters
    ----------
    dict_fname : str (optional, default: 'journal_year_articles.pickle')
        File name into which the dict will be pickled.
    mat_fname : str (optional, default: 'journal_x_year.pickle')
        File name into which the matrix (as numpy array) will be pickled.
    """
    q=r"""SELECT a.j_title, a.year, count(*)
FROM (
  SELECT replace(journal.j_title,' ','_') as j_title, strftime('%Y',article.date) as year
  FROM
    article JOIN journal ON article.jid=journal.jid
) AS a
GROUP BY a.j_title,a.year
"""
    dbfname = 'isi-orig.sqlite'
    db = sqlite3.connect(join(dblocation,dbfname))
    cur = db.cursor()
    cur.execute("attach database '%s' as 'journals'"%join(dblocation,'journal.sqlite'))
    cur.execute(q)
    jya = defaultdict(dict)
    for line in cur:
        if line[1]==None:
            continue
        journal = str(line[0])
        year = int(line[1])
        jya[journal][year] = line[2]
    db.close()
    pickle.dump(jya, open(join(dsets_loc,jya_fname), 'w'))
    print "journals:year:#articles dict saved to '%s'"%join(dsets_loc,jya_fname)
    
    # year:journal:article version
    yja = defaultdict(dict)
    for journal,years in jya.iteritems():
        for year,articles in years.iteritems():
            yja[year][journal] = articles
    pickle.dump(yja, open(join(dsets_loc,yja_fname), 'w'))
    print "year:journal:#articles dict saved to '%s'"%join(dsets_loc,yja_fname)
    
    journals = sorted(jya)
    mat = np.zeros((len(jya),end-start), dtype=int)
    for j,journal in enumerate(journals):
        years = jya[journal]
        for year,articles in years.iteritems():
            mat[j,year-start] = articles
    pickle.dump((journals,start,mat), open(join(dsets_loc,mat_fname),'w'))
    print "[journal,year]:#articles matrix saved to '%s'"%join(dsets_loc,mat_fname)

#=================================
def save_subfield_dict(field='subfield'):
    """Reads journal:(sub)fields information from database and saves to pickled dict."""
    db = sqlite3.connect(join(dblocation, 'journal.sqlite'))
    cur = db.cursor()
    fields = defaultdict(set)
    if field=='subfield':
        cur.execute("SELECT replace(journal.j_title,' ','_'), replace(subfield.sf,' ','_') FROM subfield JOIN journal ON subfield.jid=journal.jid")
    if field=='field':
        cur.execute("SELECT replace(journal.j_title,' ','_'), replace(majorsubfield.sf,' ','_') FROM majorsubfield JOIN (subfield JOIN journal ON subfield.jid=journal.jid) ON majorsubfield.sf=subfield.sf")
    for j,sf in cur:
        fields[j].add(sf)
    pickle.dump(fields, open(join(dsets_loc, field+'s.pickle'), 'w'))
    print "dict saved to file '%s'"%join(dsets_loc, field+'s.pickle')

#==============================================================================
def get_JGP(*args, **kwargs):
    import gttools as gtt
    sample_name = kwargs.get('sample_name','0.05')
    jnp = gtt.Prediction(*args, **kwargs)
    jnp.directed = True
    jnp._fast_node_prob = False
    jnp.sample_name = sample_name
#    jnp.layer = 0
    jnp.meta_bidir = True
    return jnp
#=================================
def prepare_jnets(dsets, field_layer=False):
    """Loads isi_subfields layer and saves g in gt format."""
    for dset in dsets:
        jnp = get_JGP(dset,
                edgelist=join(dsets_loc,dset,'graph-orig.txt'),
                sample_name='all', removed_nodes=0, meta_multiple='one',
                simple_graph=False, weighted=True, directed=True)
        jnp.add_layer(join(dsets_loc,dset,'comm-orig-jnet.txt'),
                      layer_name='isi_subfields')
        if field_layer:
            jnp.add_layer(join(dsets_loc,dset,'comm-orig-flds.txt'),
                               layer_name='isi_fields')
        jnp.saveg()
#=================================
def get_multiples(*args, **kwargs):
    multiples = kwargs.get('multiples',
                ['one','avg_in','avg_total','sv_in','sv_out','sv_total'])
    for mult in multiples:
        jnp = get_JGP(*args, **kwargs)
        jnp.meta_weight = mult
        yield jnp

#=================================
def collect_basic_stats(gs, pickle_fname=''):
    """Uses Dataset class from gttools.py"""
    from graph_tool.stats import label_parallel_edges
    import pickle
    try:
        stats = pickle.load(open(pickle_fname, 'r'))
    except:
        stats = {}
    for g in gs:
        dsetname = g.gp['dsetname']
        if stats.has_key(dsetname):
            continue
        stats[dsetname] = dsst = {}
        dsst['n'] = g.num_vertices()
        dsst['e'] = g.num_edges()
        dsst['degree_in'] = np.bincount(g.degree_property_map('in').a)
        dsst['degree_out'] = np.bincount(g.degree_property_map('out').a)
        dsst['degree_total'] = np.bincount(g.degree_property_map('total').a)
        prls_count = np.bincount(label_parallel_edges(g).a)
        mult = [np.where(prls_count>=count)[0].shape[0]+1 for count in range(len(prls_count))]
        dsst['edge_mult_dist'] = np.bincount(mult)
        print "stats for %s collected"%dsetname
        if pickle_fname:
            pickle.dump(stats, open(pickle_fname, 'w'))
            print "  --> stats pickle saved"
    return stats

#==============================================================================
def collect_basic_stats2(names, pickle_fname=dsets_loc+'stats.pickle'):
    """Uses networkx."""
    import pickle
    from collections import defaultdict
    def get_G(G, dsetname):
        if G==None:
            G = get_networkx(dsetname)
        return G
    try:
        stats = pickle.load(open(pickle_fname, 'r'))
        print "stats loaded from file"
    except:
        stats = defaultdict(dict)
    for dsetname in names:
        dsst = stats[dsetname]
        G = None
        if not dsst.has_key('n') or not dsst.has_key('e'):
            G = get_G(G, dsetname)
            print "collecting n, e ..."
            dsst['n'] = G.number_of_nodes()
            dsst['e'] = G.number_of_edges()
        if not dsst.has_key('degree_in'):
            G = get_G(G, dsetname)
            print "collecting degree_in..."
            dsst['degree_in']    = np.bincount([d[1] for d in\
                                   G.in_degree_iter(weight='mult')])
        if not dsst.has_key('degree_out'):
            G = get_G(G, dsetname)
            print "collecting degree_out..."
            dsst['degree_out']   = np.bincount([d[1] for d in\
                                   G.out_degree_iter(weight='mult')])
        if not dsst.has_key('degree_total'):
            G = get_G(G, dsetname)
            print "collecting degree_total..."
            dsst['degree_total'] = np.bincount([d[1] for d in\
                                   G.degree_iter(weight='mult')])
        if not dsst.has_key('edge_mult_dist'):
            G = get_G(G, dsetname)
            print "collecting edge_mult_dist..."
            dsst['edge_mult_dist'] = np.bincount([e[2]['mult'] for e in\
                                     G.edges_iter(data=True)])
        if not dsst.has_key('self_loop_mult_dist'):
            G = get_G(G, dsetname)
            print "collecting self_loop_mult_dist..."
            dsst['self_loop_mult_dist'] = np.bincount([e[2]['mult'] for e in\
                                     G.selfloop_edges(data=True)])
        print "stats for %s collected"%dsetname
        if pickle_fname and G!=None:
            pickle.dump(stats, open(pickle_fname, 'w'))
            print "  --> stats pickle saved"
    return stats

#==============================================================================
def graph_iterator(names):
    import gttools as gtt
    import gc
    for name in names:
        print '\n%s:\n%s'%(name,'='*(len(name)+1))
        gc.collect()
        jn = gtt.Dataset(name, edgelist=dsets_loc+'%s/graph-orig.txt'%name,
                         weighted=True, simple_graph=False)
        jn.directed = True
        jn.saveg(overwrite=True)
        yield jn.g

#==============================================================================
def get_networkx(name):
    """Reads from original edgelist and returns networkx object."""
    import networkx as nx
    import gc
    print '\n%s:\n%s'%(name,'='*(len(name)+1))
    gc.collect()
    nxgraphfile = join(dsets_loc,'%s/graph.nx.pickle'%name)
    try:
        G = pickle.load(open(nxgraphfile, 'r'))
        print "networkx file loaded from '%s'"%nxgraphfile
    except:
        edgelistfile = join(dsets_loc,'%s/graph-orig.txt'%name)
        print "reading from edgelist file..."
        edges = [l.split() for l in open(edgelistfile,'r').readlines() if l[0]!='#']
        edges = [(l[0],l[1],int(l[2])) for l in edges]
        print "loading into networkx..."
        G = nx.DiGraph(name=name)
        G.add_weighted_edges_from(edges, weight='mult')
        print "dumping networkx file..."
        pickle.dump(G, open(nxgraphfile, 'w'))
        print "networkx file saved to '%s'"%nxgraphfile
    return G

#==============================================================================
def print_basic_stats(stats):
    stats_report = {}
    for dset in sorted(stats.keys()):
        stat = stats[dset]
        stats_report[dset] =\
            "%s:\n n:   %d\n e:   %d\n ind: %f\n od:  %f\n td:  %f\n em:  %f"%\
            (dset,stat['n'],stat['e'],
             np.average(range(stat['degree_in'].shape[0]),
                        weights=stat['degree_in']),
             np.average(range(stat['degree_out'].shape[0]),
                        weights=stat['degree_out']),
             np.average(range(stat['degree_total'].shape[0]),
                        weights=stat['degree_total']),
             np.average(range(stat['edge_mult_dist'].shape[0]),
                        weights=stat['edge_mult_dist']))
        print stats_report[dset]
    return stats_report

#==============================================================================
def plot_basic_stats(stats, dsets=None,
                     plot_loc=join(dsets_loc,'plots'), fnames_prefix='',
                     prob_dist=False):
    """Plots basic statistics.

    Parameters
    ----------
    stats : dict
        Dict containing data. Use the result of collect_basic_stats2.
    dsets : list (optional, default: None)
        Use this list of dsets if given, otherwise use all available in stats.
    plot_loc : str
        Location to save plots.
    fnames_prefix : str
    prob_dist : bool
        True  - Plot probability density.
        False - Plot expected counts.

    Notes
    -----
    Counts for value zero are also shown before value=1, separated by gray 
    vertical line.
    """
    from pcd.support import matplotlibutil
    import matplotlib
#    assert np.__version__ >= '1.11.0'
    import binner
    from scipy.stats.mstats import gmean
#    measures = []
    measures = ['degree_in', 'degree_out', 'degree_total',
                'edge_mult_dist', 'self_loop_mult_dist']
    colors  = ['b','g','r','c','m','y','k']
    cm = matplotlib.cm.get_cmap('gist_rainbow')
    markers = ['o','^','s','D','v','h','>','*','<']
    # location of the zero points
    zero_loc = .2
    # vertical separator location
    vert_line = gmean((zero_loc, 1))
    if dsets==None:
        dsets = sorted(stats.keys())
    
    # plotting the rest
    for mes in measures:
        if prob_dist:
            plot_fname = join(plot_loc, fnames_prefix+'dist_'+mes+'.png')
        else:
            plot_fname = join(plot_loc, fnames_prefix+'counts_'+mes+'.png')
        ax,extra = matplotlibutil.get_axes(plot_fname, figsize=(9,6))
        for i,dset in enumerate(dsets):
            data_orig = stats[dset][mes]
            total_sum = 1.
            if prob_dist:
                total_sum = float(sum(data_orig))
            data = [(c,d) for c,d in enumerate(data_orig) if d>0]
            bins = binner.Bins(int, 0, max(data)[0], 'linlog', 1.5)
            bin_counter = bins.bin_sum_divide(data)
            ax.plot(np.concatenate(([zero_loc,.7],bins.centers[1:])),    # add new x positions
                    np.ma.concatenate((data_orig[:1],          # zero value
                                       np.ma.masked_all((1,)), # blank spot
                                       bin_counter[1:]))/total_sum,
                    c=cm(1.*i/len(dsets)),
                    marker=markers[i%len(markers)],
                    label=dset)
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_xlabel(mes.replace('_dist',''))
        if prob_dist:
            ax.set_ylabel('probability density')
        else:
            ax.set_ylabel('count')
        old_xlim = ax.get_xlim()
        # add a separator between zero and the rest
        ax.plot([vert_line,vert_line], ax.get_ylim(), linestyle='-', c='silver',
                linewidth=4)
        # first we need to draw figure to instantiate tick labels
        extra[1].draw()
        # moving the first major tick
        matl = ax.xaxis.get_majorticklocs()
#        print "major_tick_locs:",matl
#        matl = np.concatenate(([zero_loc],matl[matl>=1]))
        matl = np.where(matl==.1, .2, matl)
#        print "major_tick_locs:",matl
        ax.xaxis.set_ticks(matl)
        # removing minor ticks <= 1
        mitl = ax.xaxis.get_minorticklocs()
#        print "minor_tick_locs:",mitl
        mitl = mitl[mitl>1]
#        print "minor_tick_locs:",mitl
        ax.xaxis.set_ticks(mitl, minor=True)
        # adding a label for zero value
        labels = [item.get_text() for item in ax.get_xticklabels()]
#        print "labels:",labels
        labels = ['zero' if l==u'$\\mathdefault{10^{-1}}$' else l for l in labels]
        ax.set_xticklabels(labels)
        ax.set_xlim(old_xlim)
        ax.legend(fontsize='small', numpoints=1, fancybox=True, framealpha=.5)
        matplotlibutil.save_axes(ax, extra)
        print "plot saved to %s"%plot_fname
#        raise Exception
    
    # plotting n and e
    ns = [[stats[dset]['n'] for dset in dsets], 'nodes']
    es = [[stats[dset]['e'] for dset in dsets], 'edges']
    plot_fname = join(plot_loc, fnames_prefix+'basic.png')
    ax,extra = matplotlibutil.get_axes(plot_fname, figsize=(9,6))
    for i,(data,mes) in enumerate((ns,es)):
        ax.plot(data, c=colors[i%len(colors)],
                marker=markers[i%len(markers)],
                label=mes)
    ax.set_yscale('log')
    ax.set_xlabel('datasets')
    ax.set_ylabel('count')
    ax.set_xticks(range(len(dsets)))
    ax.set_xticklabels(dsets, rotation=45)
    ax.set_xlim(-.05*(len(dsets)-1), 1.05*(len(dsets)-1))
    ax.legend(loc='upper left', numpoints=1, fancybox=True, framealpha=.5)
    matplotlibutil.save_axes(ax, extra)
    print "plot saved to %s"%plot_fname
    

#==============================================================================
def calc_measure(jnx, measure):
    """Calculates reqested measure on the dict of networkx graphs.

    Parameters
    ----------
    jnx : dict
        Dict of networkx objects: dsetname:nx.Graph
    measure : str
        Measure to be calculated.

    Returns
    -------
    Dict of the same form as jnx, but with results instead of graphs, in the form:
        reverse sorted list of tuples: (value, node or edge)
    """
    dsets = sorted(jnx.keys())
    if measure in ['out_degree','in_degree','degree']:
        meas = {dset:sorted([(n,d) for n,d in getattr(jnx[dset],measure+'_iter')(weight='mult')], key=lambda x:x[1], reverse=True) for dset in dsets}
    elif measure=='self_loop':
        meas = {dset:sorted([(e[0],e[2]) for e in jnx[dset].selfloop_edges(data='mult')], key=lambda x:x[1], reverse=True) for dset in dsets}
    elif measure=='edge_mult':
        meas = {dset:sorted(jnx[dset].edges_iter(data='mult'), key=lambda x:x[2], reverse=True) for dset in dsets}
    else:
        raise RuntimeError("unknown measure '%s'"%measure)
    return meas
#=================================
def make_report(meas, measure, sample_n=10):
    """Given measure data from calc_measure make a short text report.

    Parameters
    ----------
    meas : dict
        The data to be used.
    measure : str
        The name of the measure.
    sample_n : int (optional, default: 10)
        Number of maximum values to report.
    """
    dsets = sorted(meas.keys())
    report = "%s:\n"%measure
    for dset in dsets:
        report += "\n%s:\n%s\n"%(dset,'\n'.join(["%8d: %s"%(t[-1],' | '.join(t[:-1])) for t in meas[dset][:sample_n]]))
    return report
#=================================
def append_report(stat_data, sample_n=10, exceptions={}):
    measures = stat_data.keys()
    sample_n = {measure:exceptions.get(measure, sample_n) for measure in measures}
    for measure in measures:
        data = stat_data[measure]
        stat_data[measure] = [data, make_report(data, measure, sample_n[measure])]
#=================================
def save_measure_reports(stat_data):
    measures = stat_data.keys()
    for measure in measures:
        with open(join(dsets_loc,'report_max_%s.txt'%measure),'w') as f:
            f.write(stat_data[measure][1])

#==============================================================================
#==============================================================================
def collect_jnps(dsets, jnps=None, layer='isi_subfields', meta_weights=['one'],
                 recollect=False):
    """Populates jnps dict with Prediction objects.
    
    Paramerers
    ----------
    dsets : list
        List of dset names whose data to colect.
    jnps : dict (optional, default: None)
        A dictionary of Prediction objects. Keys are "dset-meta_weight" pairs.
        If not provided, new will be created.
    meta_weights : list (optional, default: ['one'])
        A list of meta_weights (to be matched with dsets list).
    
    Returns
    -------
    jnps : dict
        A dictionary of Prediction objects. Keys are "dset-meta_weight" pairs.
    """
    import gttools as gtt
    if not isinstance(jnps, dict):
        jnps = {}
    for meta_weight in meta_weights:
        for dset in dsets:
            dsetmw = dset+'-'+meta_weight
            if dsetmw in jnps and not recollect:
                continue
            jnps[dsetmw] = gtt.Prediction(dset, removed_nodes=0,
                                          sample_name='all', layer=layer,
                                          meta_weight=meta_weight)
    return jnps
#=================================
def make_blocks_overview(dsets, jnps, meta_weights):
    """Create neat overview of blocks in a list of datasets.

    Parameters
    ----------
    dsets : list
        List of dset names whose data to colect.
    jnps : dict
        A dictionary of Prediction objects.
    meta_weights : list
        A list of meta_weights (to be matched with dsets list).

    Returns
    -------
    report : string
        A nice report with the following format:
            meta_weight: <meta_weight>
            <dset>-<meta_weight> (dv, mv): dbdo (dbdm, mbd)
            ...
        Where the acronims are numbers of:
            dv   - data vertices
            mv   - meta vertices
            dbdo - data blocks in data only network
            dbdm - data blocks in data meta network
            mbd  - meta blocks in data meta network
        This is listed for all dsets and repeated in groups by meta_weights.
    """
    report = ''
    for meta_weight in meta_weights:
        report += "\nmeta_weight: %s\n"%meta_weight
        for dset in dsets:
            dsetmw = dset+'-'+meta_weight
            report +=  "%s (%d, %d): %d %s\n"%(dsetmw, jnps[dsetmw].u.num_vertices(), jnps[dsetmw].u_tg.num_vertices()-jnps[dsetmw].u.num_vertices(), jnps[dsetmw].blocks1, str(jnps[dsetmw].blocks2))
    print report
    return report
#=================================
def make_tag_groups(dsets, jnps, meta_weights):
    """Collects tags in tag blocks from data_meta_state.

    Parameters
    ----------
    dsets : list
        List of dset names whose data to colect.
    jnps : dict
        A dict with items:
            dset:Prediction_object
    meta_weights : list
        A list of meta_weights (to be matched with dsets list).

    Returns
    -------
    blocks : dict
        A dictionary containing tag blocks:
            dsetmw:{tag_group_SN:{<tags>...}}
    """
    dsetmws = [dset+'-'+meta_weight for dset,meta_weight in product(dsets,meta_weights)]
    blocks = {dsetmw:defaultdict(set) for dsetmw in dsetmws}
    for dsetmw in dsetmws:
        jnp = jnps[dsetmw]
        block_vp = jnp.data_meta_state.levels[0].get_blocks()
        for v in jnp.u_tg.vertices():
            if jnp.u_tg.vp['kind'][v]:
                blocks[dsetmw][block_vp[v]].add(jnp.u_tg.vp['name'][v])
    return blocks
#=================================
def make_tag_groups_report(blocks, dsets, meta_weights):
    """Formats the results of make_tag_groups nicely.

    Paramaters
    ----------
    blocks : dict
        The result of make_tag_groups
    dsets : list
        List of dset names whose data to colect.
    meta_weights : list
        A list of meta_weights (to be matched with dsets list).

    Returns
    -------
    report : string
        Basically a PrettyPrinted 'blocks'.
    """
    import pprint
    pp = pprint.PrettyPrinter(indent=2)
    report = ''
    for dset in dsets:
        for meta_weight in meta_weights:
            dsetmw = dset+'-'+meta_weight
            report += "\n%s:\n%s\n"%(dsetmw, pp.pformat(dict(blocks[dsetmw])))
    print report
    return report
#=================================
def make_journal_groups(dsets, jnps, meta_weights):
    """Collects journals from data part of the data_meta_state.

    Parameters
    ----------
    dsets : list
        List of dset names whose data to colect.
    jnps : dict
        A dict with items:
            dset:Prediction_object
    meta_weights : list
        A list of meta_weights (to be matched with dsets list).

    Returns
    -------
    blocks : dict
        A dictionary containing journal blocks and their subfields:
            dsetmw:
            { journal_group_SN:
              { ((<journals>...), [(occurence,subfield)...]) }}
    """
    from collections import Counter
    dsetmws = [dset+'-'+meta_weight for dset,meta_weight in product(dsets,meta_weights)]
    blocks = {dsetmw:defaultdict(list) for dsetmw in dsetmws}
    for dsetmw in dsetmws:
        jnp = jnps[dsetmw]
        block_vp = jnp.data_meta_state.levels[0].get_blocks()
        for v in jnp.u_tg.vertices():
            if not jnp.u_tg.vp['kind'][v]:
                blocks[dsetmw][block_vp[v]].append((jnp.u_tg.vp['name'][v],
                                    jnp.u_tg.vp['isi_subfields'][v]))
        for block in blocks[dsetmw].iterkeys():
            journals,subfields = zip(*blocks[dsetmw][block])
            subfields2 = []
            for sf in subfields:
                subfields2.extend(sf)
            cnt = Counter(subfields2)
            sf_dist = sorted([(c,sf) for sf,c in cnt.iteritems()], reverse=True)
            blocks[dsetmw][block] = (journals, sf_dist)
    return blocks
#=================================
def make_block_network(jnp, made_from, net_format='el'):
    """Makes a network of blocks.

    Parameters
    ----------
    jnp : :class:`gttg.Prediction'
        Journal network Prediction object from which the blocks will be extracted.
    made_from : str
        Type of the network. It can be:
        'jd'    - journal network from data_state
        'jdm'   - journal networs from data_meta_state
        'sfjd'  - subfield network from jd
        'sfjdm' - subfield network from jdm
        'sfdm'  - subfield network from data_meta_state
    net_format : str (optional, default: 'el')
        In what format should the network be returned. Options:
        'el'    - edgelist
        'gt'    - Graph-tool
        'nx'    - Networkx
    """
    

def make_subfield_network(sf_cooccurences):
    from itertools import combinations
    links = set()
    for block in [[s[1] for s in sfs] for jrns,sfs in sf_cooccurences.itervalues()]:
        for link in combinations(block, 2):
            if link:
                links.add(tuple(link))
    return links


#==============================================================================
#===========================   partitions   ===================================
#==============================================================================
class Fm(object):
    """Contains general (sub)fields maps.

    Attributes
    ----------
    subfield_map : dict
        subfield->field map.
    all_fields : list
        Sorted list of all fields.
    field_map : defaultdict(set)
        field->set(subfields) map.
    all_sf : set
        All subfields.
    subfields : dict
        subfields[journal] = set(subfields)
    fields : dict
        fields[journal] = set(fields)
    yja : dict
        year:journal:#articles
    ysfj_set : dict
        year:subfield:set(journals)
    ysfj : dict
        year:subfield:#journals
    yfj_set : dict
        year:field:set(journals)
    yfj : dict
        year:field:#journals
    """
    def __init__(self,
            field_map_file='/m/cs/scratch/isi/darko/majorsubfield.table.txt',
            j_subfields_file=join(dsets_loc, 'subfields.pickle'),
            yja_fname=join(dsets_loc,'year_journal_articles.pickle')):
        """
        Arguments
        ---------
        field_map_file : string (optional, default: '/m/cs/scratch/isi/darko/majorsubfield.table.txt')
            The location of the field->subfield map file.
        """
        # subfield:field
        self.subfield_map = {}
        with open(field_map_file,'r') as fmf:
            lines = fmf.read().splitlines()
            for line in lines:
                line = line.split('|')
                self.subfield_map[line[1].replace(' ','_')] = line[0]
        self.all_fields = list(sorted(set(self.subfield_map.itervalues())))

        # field:set(subfields)
        self.field_map = defaultdict(set)
        self.all_sf = set()
        for sf,f in self.subfield_map.iteritems():
            self.field_map[f].add(sf)
            self.all_sf.add(sf)
        
        # journal:subfields
        self.subfields = pickle.load(open(j_subfields_file, 'r'))
        # journal:fields
        self.fields = defaultdict(set)
        for j,sfs in self.subfields.iteritems():
            self.fields[j].update(set([self.subfield_map[sf] for sf in sfs]))
        
        # year:journal:#articles
        with open(yja_fname,'r') as yjaf:
            self.yja = pickle.load(yjaf)
        
        # year:subfield:set(journals)
        self.ysfj_set = {}
        # year:subfield:#journals
        self.ysfj = {}
        for year in self.yja:
            self.ysfj_set[year] = defaultdict(set)
            for j in self.yja[year]:
                for sf in self.subfields[j]:
                    self.ysfj_set[year][sf].add(j)
            
            self.ysfj[year] = {sf:len(js) for sf,js in\
                               self.ysfj_set[year].iteritems()}
        
        # year:field:set(journals)
        self.yfj_set = {}
        # year:field:#journals
        self.yfj = {}
        for year in self.yja:
            self.yfj_set[year] = \
                {f:set.union(*[self.ysfj_set[year].get(sf,set()) for sf in sfs])\
                              for f,sfs in self.field_map.iteritems()}
            self.yfj_set[year] = {f:js for f,js in self.yfj_set[year].iteritems()\
                                   if len(js)>0}
            self.yfj[year] = {f:len(js) for f,js in self.yfj_set[year].iteritems()}

    
#=================================
class JnpCmtys(object):
    """Contains clusters of tags as Communities objects.

    Attribute naming conventions
    ----------------------------
    The following is the naming convention for the attributes in this class.
    Nodes:
        'f'     - fields
        'sf'    - subfields
        'j'     - journals
    Source (what is used to build the clusters):
        'm'     - preexisting field->subfield map
        'ds'    - data_state
        'dms'   - data_meta_state
    Source2 (for data_meta_state there are two ways to build tag blocks):
        'j'     - from journal blocks
        't'     - from tag blocks
    Source3 (for 'j' nodes):
        'f'     - fields
        'sf'    - subfields
    Miscellaneous:
        '1'     - string names are mapped to integers (in case of 'j' nodes,
                  this has no effect)

    The attribute names are constructed by combining the codes from the
    following list, like this:

        Nodes_Source[_Source2|_Source3][Miscellaneous]

    Graphically, as flow diagram from left to right:

                    /--- _m   --+------\
          /- f  -\ /  /- _ds  -/        \
        -+-- sf --+--+-- _dms --+-- _j --+-+-+-------+-
          \         /            \- _t -/ /   \- 1 -/
           \- j ---+-+-- _f  --+---------/
                      \- _sf -/

    pcd.cmty.Communities objects implemented so far
    -----------------------------------------------
    sf_m, sf_m1
        From the input argument field_map (full and int names)
    sf_dms_t, sf_dms_t1
        Subfield communities inferred from tag blocks in data_meta_state.
    sf_ds, sf_ds1
        Subfield communities inferred from journal blocks in data_state.
        They are unweighted and inclusive, meaning that subfield will be present
        in the block (communities) if it appears in any number of nodes
        belonging to this block. This applied to any [s]f_d[m]s partition.
    f_ds
        Field communities inferred from journal blocks in data_state. 
    j_ds
        Journal communities from blocks in data_state.
    j_dms
        Journal communities from blocks in data_meta_state.
    j_f
        Journal communities inferred from fields they belong to.
    j_sf
        Journal communities inferred from subfields they belong to.
    """
    def __init__(self, field_map, jnp, ds_level=0, dms_level=0):
        """
        Arguments
        ---------
        field_map : dict
            Field:set(subfields) mapping provided externally
        jnp : Journal Prediction object
            The dataset everything revolves around.
        ds_level : int (optional, default: 0)
            The level of data_state to be used.
        dms_level : int (optional, default: 0)
            The level of data_meta_state to be used.
        """
        self.ds_level = ds_level
        self.dms_level = dms_level
        self.field_map = field_map
        self.sf_m = Cm(self.field_map)
        self.jnp = jnp
        self.sf_all = set()
        for v in self.jnp.tg.vertices():
            if not self.jnp.tg.vp.kind[v]: continue
            sf = self.jnp.tg.vp.name[v]
            self.sf_all.add(sf)
        
        # subfield_names->int map
        # (contains all the subfields, so it can be used as a general mapping)
        self._sfim = self.sf_m.nodeintmap()
        
        # cmty.Communities objects with ints instead of sf_names
        self.sf_m1 = Cm(self._sfs_to_ints(self.sf_m))
        
        self._sf_dms_t = None
        self._sf_dms_t1 = None
        self._ds_b = None
        self._dms_b = None
        self._sf_ds = None
        self._sf_ds1 = None
        self._f_ds = None
        self._j_ds = None
        self._j_dms = None
        self._j_f = None
        self._j_sf = None
    
    #=================================
    @property
    def ds_b(self):
        if self._ds_b==None:
            self._ds_b = self.jnp.data_state.levels[self.ds_level].b
        return self._ds_b
    @property
    def dms_b(self):
        if self._dms_b==None:
            self._dms_b = self.jnp.data_meta_state.levels[self.dms_level].b
        return self._dms_b
    @property
    def sf_dms_t(self):
        if self._sf_dms_t==None:
            _sf_dms_t = defaultdict(set)
            for v in self.jnp.tg.vertices():
                if not self.jnp.tg.vp.kind[v]: continue
                sf = self.jnp.tg.vp.name[v]
                _sf_dms_t[self.dms_b[v]].add(sf)
            self._sf_dms_t = Cm(_sf_dms_t)
        return self._sf_dms_t
    @property
    def sf_dms_t1(self):
        if self._sf_dms_t1==None:
            self._sf_dms_t1 = Cm(self._sfs_to_ints(self.sf_dms_t))
        return self._sf_dms_t1
    @property
    def sf_ds(self):
        if self._sf_ds==None:
            self._sf_ds = defaultdict(set)
            for v in self.jnp.g.vertices():
                sf = set(self.jnp.g.vp['isi_subfields'][v])
                if sf:
                    self._sf_ds[self.ds_b[v]].update(sf)
            self._sf_ds = Cm(self._sf_ds)
        return self._sf_ds
    @property
    def sf_ds1(self):
        if self._sf_ds1==None:
            self._sf_ds1 = Cm(self._sfs_to_ints(self.sf_ds))
        return self._sf_ds1
    @property
    def f_ds(self):
        if self._f_ds==None:
            self._f_ds = defaultdict(set)
            for v in self.jnp.g.vertices():
                f = set(self.jnp.g.vp['isi_fields'][v])
                if f:
                    self._f_ds[self.ds_b[v]].update(f)
            self._f_ds = Cm(self._f_ds)
        return self._f_ds
    @property
    def j_ds(self):
        if self._j_ds==None:
            self._j_ds = defaultdict(set)
            for v in self.jnp.g.vertices():
                b = self.ds_b[v]
                self._j_ds[b].add(int(v))
            self._j_ds = Cm(self._j_ds)
        return self._j_ds
    @property
    def j_ds1(self):
        return self.j_ds
    @property
    def j_dms(self):
        if self._j_dms==None:
            self._j_dms = defaultdict(set)
            for v in self.jnp.tg.vertices():
                if self.jnp.tg.vp.kind[v]: continue
                b = self.dms_b[v]
                self._j_dms[b].add(int(v))
            self._j_dms = Cm(self._j_dms)
        return self._j_dms
    @property
    def j_dms1(self):
        return self.j_dms
    @property
    def j_f(self):
        if self._j_f==None:
            self._j_f = defaultdict(set)
            for v in self.jnp.g.vertices():
                fs = self.jnp.g.vp['isi_fields'][v]
                for f in fs:
                    self._j_f[f].add(int(v))
            self._j_f = Cm(self._j_f)
        return self._j_f
    @property
    def j_f1(self):
        return self.j_f
    @property
    def j_sf(self):
        if self._j_sf==None:
            self._j_sf = defaultdict(set)
            for v in self.jnp.g.vertices():
                sfs = self.jnp.g.vp['isi_subfields'][v]
                for sf in sfs:
                    self._j_sf[sf].add(int(v))
            self._j_sf = Cm(self._j_sf)
        return self._j_sf
    @property
    def j_sf1(self):
        return self.j_sf
    
    #=================================
    def compare(self, cmt1, cmt2, func, intersection=True):
        """Compares two cmtys using 'func'."""
        if isinstance(func, str):
            func = getattr(cmtycmp, func)
        assert func.__module__=='pcd.cmtycmp'
        if intersection:
            return func(*self.intersected(cmt1,cmt2))
        else:
            return func(cmt1,cmt2)
    
    def intersected(self, cmt1, cmt2):
        """Returns cmtys with nodes restricted to their intersection."""
        limit_nodes = cmt1.nodes_spanned().intersection(cmt2.nodes_spanned())
        return Cm(cmt1._cmtynodes, limit_nodes=limit_nodes),\
               Cm(cmt2._cmtynodes, limit_nodes=limit_nodes)
    
    def _sfs_to_ints(self, cmt):
        return {cn:set([self._sfim[n] for n in ns]) for cn,ns in cmt.iteritems()}


#=================================
def JSD(P, Q):
    """Jensen-Shannon divergence"""
    _P = np.array(P, dtype=float) / np.linalg.norm(P, ord=1)
    _Q = np.array(Q, dtype=float) / np.linalg.norm(Q, ord=1)
    _M = 0.5 * (_P + _Q)
    return 0.5 * (entropy(_P, _M) + entropy(_Q, _M))

#=================================
class MostSimilar(object):
    def __init__(self, props):
        self.props = props
        self.sizes = {}
        self.cmtys = {}
        self.sims = {}
        self.similars = {}
        self.distances = {}
        for dset,props_dset in self.props.iteritems():
            self.sizes[dset] = {l:[ps[0] for ps in bps.itervalues()] for l,bps in self.props[dset].iteritems() if l!=0}
            self.cmtys[dset] = {}
            nodemap = pcd.cmty.Communities({k:p[6] for k,p in props_dset[0].iteritems()}).nodeintmap()
            for level, prop in props_dset.iteritems():
                self.cmtys[dset][level] = Cm({k:[nodemap[n] for n in p[6]] for k,p in prop.iteritems()})

    def calc_similarities(self, dsets=None, logspace=(0, 4.6, 20), verbose=False):
        if not dsets:
            dsets = sorted(self.props.keys())
        for dset in dsets:
            print "calculating",dset
            levels = self.sizes[dset].keys()
            self.sims[dset] = {}
            for dl in ('fields','subfields'):
                B_dl = len(self.sizes[dset][dl])
                self.sims[dset][dl] = []
                if verbose: print dl, B_dl
                for l in [l for l in levels if isinstance(l, int)]:
                    dist_dl = np.histogram(self.sizes[dset][dl], bins=np.logspace(*logspace))[0]
                    dist_l = np.histogram(self.sizes[dset][l], bins=np.logspace(*logspace))[0]
                    self.sims[dset][dl].append((l,
                        abs(len(self.sizes[dset][l]) - B_dl),
                        JSD(dist_dl, dist_l),
                        cmtycmp.NMI_LF(self.cmtys[dset][dl], self.cmtys[dset][l]),
                        len(self.sizes[dset][l])))
                    if verbose: print "%2s %5d %.4f %5d"%self.sims[dset][dl][-1]
    
    def get_most_similar(self, dsets=None):
        if not dsets:
            dsets = sorted(self.sims.keys())
        sim_levels_JSD = {'fields':[], 'subfields':[]}
        sim_levels_NMI = {'fields':[], 'subfields':[]}
        for dset in dsets:
            self.similars[dset] = {}
            for ld in ('fields','subfields'):
                most_similar = np.argmin(self.sims[dset][ld], axis=0)
                ms_count = self.sims[dset][ld][most_similar[1]][0]
                ms_JSD   = self.sims[dset][ld][most_similar[2]][0]
                most_similar = np.argmax(self.sims[dset][ld], axis=0)
                ms_NMI   = self.sims[dset][ld][most_similar[3]][0]
                self.similars[dset][ld] = (ms_count,
                                           ms_JSD,
                                           ms_NMI,
                                           ms_count - ms_JSD,
                                           ms_count - ms_NMI)
#                sims_dset_ld = zip(*self.similars[dset][ld])
                sim_levels_JSD[ld].append(self.similars[dset][ld][3])
                sim_levels_NMI[ld].append(self.similars[dset][ld][4])
        self.distances['fields'] = (Counter(sim_levels_JSD['fields']),
                                    Counter(sim_levels_NMI['fields']))
        self.distances['subfields'] = (Counter(sim_levels_JSD['subfields']),
                                       Counter(sim_levels_NMI['subfields']))


#==============================================================================
#==============================================================================
if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description="Runs wos stuff.",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("calculate", type=str,
                        help=r"""What to calculate. Implemented so far:
    threshold - make_thresholded_network""")
    parser.add_argument("-d", "--datasets", type=str,
                        help="Comma-separated list of dataset names.")
    parser.add_argument("-l", "--layer", type=str, default=0,
                        help="layer")
    parser.add_argument("-a", "--alpha", type=float, metavar="VALUE",
                        help="Value of alpha used for thresholding.")
    parser.add_argument("-s", "--save", action="store_true",
                        help="Save the new graph to disk?")

    args = parser.parse_args()
    if args.calculate=='threshold':
        if not (args.datasets and args.alpha):
            parser.print_help()
            raise RuntimeError, "Some arguments are missing!"
        dsets = args.datasets.split(',')
        print "datasets to calculate:",dsets,'\n'
#        print "args:",args
        for dset in dsets:
            print dset
#            continue
            make_thresholded_network(dset, args.alpha, save=args.save,
                                     layer=args.layer)
        





